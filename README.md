This is an OpenAI Retro Trainer for Sonic the Hedgehog using [NEAT](https://neat-python.readthedocs.io/en/latest/). It is based on a tutorial by Vedant Gupta at https://www.freecodecamp.org/news/how-to-use-ai-to-play-sonic-the-hedgehog-its-neat-9d862a2aef98/.

Just run by typing:

`python Training.py`

Requires [Gym Retro](https://retro.readthedocs.io/en/latest/index.html) installation.

Here's a video showing the installation in Windows 7:

https://youtu.be/j3eHWG2CtqU

Here's a video showing the trainer in action:

https://youtu.be/VXuTAEYJ1jg